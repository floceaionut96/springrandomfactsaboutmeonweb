package com.aboutme.randomfactsaboutme;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RandomFactsAboutMeApplication {

    public static void main(String[] args) {
        SpringApplication.run(RandomFactsAboutMeApplication.class, args);
    }

}
