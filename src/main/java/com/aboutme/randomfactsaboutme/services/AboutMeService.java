package com.aboutme.randomfactsaboutme.services;

public interface AboutMeService {

    String getRandomFact();

}
