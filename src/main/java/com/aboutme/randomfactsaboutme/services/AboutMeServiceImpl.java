package com.aboutme.randomfactsaboutme.services;


import com.aboutme.randomfactsaboutme.FactsAboutMe;
import org.springframework.stereotype.Service;

@Service
public class AboutMeServiceImpl implements AboutMeService{

    private final FactsAboutMe factsAboutMe;

    public AboutMeServiceImpl(FactsAboutMe factsAboutMe) {
        this.factsAboutMe = factsAboutMe;
    }

    @Override
    public String getRandomFact() {
        return factsAboutMe.getRandomQuote();
    }
}
