package com.aboutme.randomfactsaboutme.controllers;

import com.aboutme.randomfactsaboutme.services.AboutMeService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class AboutMeController {

    private final AboutMeService aboutMeService;

    public AboutMeController(AboutMeService aboutMeService) {
        this.aboutMeService = aboutMeService;
    }

    @RequestMapping({"/",""})
    public String showFactAboutMe(Model model){

        model.addAttribute("randomFact",aboutMeService.getRandomFact());

        return "index";

    }
}
