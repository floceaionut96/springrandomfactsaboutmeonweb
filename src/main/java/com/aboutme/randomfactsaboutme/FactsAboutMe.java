package com.aboutme.randomfactsaboutme;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
@Component
public class FactsAboutMe {

    private List<String> someFacts = new ArrayList(List.of("My name is Flocea Ionut!",
            "I have started learning to code about a year ago!",
            "My first coding language was C++!",
            "The main thing I do in my free time is sport: Football and tenis.I also like to play games with my friends.",
            "I like to read and learn about automotive.",
            "Java is my favorite programming language because it uses OOP.",
            "I am trying to learn Spring Framework and this is one of my first ever Web project.We all start somewhere :)",
            "I think that I learn new things very fast.I hope it will be the same with Spring.",
            "I have very strong Math skills :), yes I can prove it!",
            "I am a dog lover and actually a cat lover too!",
            "For the moment i work in IT for BMW group in Germany as a support analyst.",
            "I have some knowledge about Operating Systems.Mostly on Windows and Linux even tho now I am using MacOS",
            "I always like to learn new things.",
            "At the beginning I didn't like writing code at all.I remember that in HighSchool I was playing games on computer in course time.",
            "I am a student in 3rd year at  Electrical Engineering and Applied Informatics"));

    public String getRandomQuote(){
        return someFacts.get(ThreadLocalRandom.current().nextInt(0,someFacts.size()));
    }
}
